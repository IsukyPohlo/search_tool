package main

import (
	"fmt"
	"strings"
)

func main() {

	inputText := "numero: 5512345678; agrea: uwu, ewe"
	fmt.Println(inputText, " -> ")
	query := tradSelect(inputText)

	fmt.Println(query)

}

func tradSelect(inc string) string {
	/* Funcion: tradSelect.
	// 			Traduce el mensaje para hacer el Query para PostgreSQL
	//
	// Sintaxis:
	//			<campo>: <argumento1> <argumento2> ...
	//
	// Argumentos:
	//			campo [ String ]: El campo de la tabla dentro del cual buscar los argumentos
	//
	//			argumento(s) [ String ]: El texto a buscar dentro del campo.
	//									Se traduce el * como % (comodin de postgreSQL)
	*/

	// TO DO: Entre comillas que los acentos de ignoren.

	QueryAcum := ""
	QueryTrad := ""
	Q_head := "SELECT * FROM tabla WHERE "   //Primera parte del query
	In := strings.Replace(inc, "*", "%", -1) //Reemplaza los * por % [los comodines para postgreSQL]
	Sents := strings.Split(In, ";")          //Divide el mensaje por "enunciados" usando ";" como division
	longSents := len(Sents)                  //Cantidad de enunciados a hacer
	//fmt.Println("longSetns", longSents)

	for j := 0; j < longSents; j++ {

		Sent := strings.Split(Sents[j], ":") //Divide el enunciado en dos utilizando ":" como division
		Q_Field := Sent[0]                   //Primera parte del enunciado es el campo en el cual buscar
		Args := Sent[1]                      //Segunda parte del enunciado son los argumentos

		Arg := strings.Split(Args, ",") //Separa los argumentos utilizando "," como division
		long := len(Arg)                //Determina el numero de argumentos usando "," como separacion

		QueryAcum += " ( "

		for i := 0; i < long; i++ {

			QueryArg := strings.TrimSpace(Arg[i])          //Quita los espacios que pudieran estar en el argumento
			QueryAcum += Q_Field + " = '" + QueryArg + "'" //Junta todas las partes en un acumulador

			if i < long-1 && long != 1 { //Agrega un "OR" al acumulador excpeto en la ultima iteracion o si solo es un argumento
				QueryAcum += " OR "
			}
		}

		if j < longSents-1 { //Agrega un ") AND" al acumulador excpeto en la ultima iteracion en la que solo coloca un ")"
			QueryAcum += ") AND"
		} else {
			QueryAcum += " ) "
		}

	}

	QueryTrad = Q_head + QueryAcum //Junta todas las partes

	return QueryTrad
}
